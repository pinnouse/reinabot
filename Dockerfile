FROM gcr.io/google_appengine/nodejs

RUN apt-get update && \
    apt-get install -y libav-tools && \
    apt-get clean && \
    rm /var/lib/apt/lists/*_*

COPY . /app/

RUN npm install --unsafe-perm || \
  ((if [ -f npm-debug.log ]; then \
      cat npm-debug.log; \
    fi) && false)

CMD npm start