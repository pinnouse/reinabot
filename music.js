var request = require('superagent');
const config = require('./config.json');

const ytdl = require('ytdl-core');

const API_KEY = config.gapi;
const WATCH_VIDEO_URL = 'https://www.youtube.com/watch?v=';

var watchVideoUrl = WATCH_VIDEO_URL;

const prefix = config.prefix;

let queue = [];
const queueMax = 101; //Add 1 for the current song playing
let songs = [];
let songsAdded = 0;

let dispatcher = [];

var bot = null;

var lastMessage = null;

var mVolume = 10;

var lastSong = [];

var voiceConnection = [];

exports.getSongs = function() {
  return songs;
}

exports.message = function(msg) {
  lastMessage = msg;
}

exports.bot = function(client) {
  bot = client;
}

exports.getQueue = function() {
  return queue[lastMessage.guild.id];
}

function getChannelByName(channelName) {
  var channel = bot.channels.find(val => val.name === channelName);
  return channel;
}

function search(query) {
  lastMessage.channel.startTyping();
  if (!queue[lastMessage.guild.id] && queue[lastMessage.guild.id] == null)
    queue[lastMessage.guild.id] = [];

  if (queue[lastMessage.guild.id].length < queueMax) {
    var requestUrl = 'https://www.googleapis.com/youtube/v3/search' + `?q=${escape(query)}&key=${API_KEY}&maxResults=5&part=snippet`;

    request(requestUrl, (err, response) => {
      if (!err && response.statusCode === 200) {
        var body = response.body;
        if (body.items.length < 1) {
          lastMessage.channel.send("I couldn't find anything on YouTube that matches that!");
          return;
        }

        songs = [];
        for (let i = 0; i < body.items.length; i++) {
          let item = body.items[i];
          if (item.id.kind === 'youtube#video') {
            songs.push({
              'videoId': item.id.videoId,
              'videoName': item.snippet.title,
              'videoLength': ["00", "00"]
            });
          }
        }

        for (let i = 0; i < songs.length; i++) {
          getLength(songs[i].videoId, i, query);
        }
      } else {
        lastMessage.channel.send("Unexpected error occured when searching video.\nError: " + err + "\nCode: " + response.statusCode);
      }
    });
  } else {
    lastMessage.channel.send("Onii chan, やめろ. I can only remember " + (queueMax - 1) + " songs at a time.");
  }
}

function listSongs(query) {
  songsAdded = 0;
  let songList = "";
  for (let i = 0; i < songs.length; i++) {
    songList += "` " + (i + 1) + ": ` `" + songs[i].videoLength[0] + ":" + songs[i].videoLength[1] + "` - " + songs[i].videoName;
    if (i < songs.length - 1)
      songList += "\n";
  }

  if (query.startsWith('https://') && query.contains(songs[1].videoId)) {
    selectSong(1);
    lastMessage.channel.stopTyping();
  } else if (songs.length == 1)
    lastMessage.channel.send("**Result for:** " + query + "\n**Use** `" + prefix + "select 1` or `" + prefix + "sel 1` **to play the following song.**\n" + songList)
      .then(message => lastMessage.channel.stopTyping());
  else
    lastMessage.channel.send("**Results for:** " + query + "\n**Use** `" + prefix + "select <1-" + songs.length + ">` or `" + prefix + "sel <1-" + songs.length + ">` **to choose the song to play.**\n" + songList)
      .then(message => lastMessage.channel.stopTyping());

}

//Adds the video length value to the song
function addLength(vals, ind, query) {
  songsAdded ++;
  songs[ind].videoLength = vals;
  if (songsAdded >= songs.length)
    listSongs(query);
}

//Gets the length of the video
function getLength(vidId, ind, query) {
  var requestUrl = 'https://www.googleapis.com/youtube/v3/videos' + `?id=${vidId}&key=${API_KEY}&part=contentDetails`;
  let returnVal = [];
  returnVal[0] = "";
  returnVal[1] = "";

  request(requestUrl, (err, response) => {
    if (!err && response.statusCode === 200) {
      //console.log(response.body.items[0].contentDetails.duration);
      let dur = response.body.items[0].contentDetails.duration;
      let min = true;
      let hour = false;
      let hMsg = "";

      for (let i = 0; i < dur.length; i++) {
        if (isInt(dur[i])) {
          if (min)
            returnVal[0] += dur[i];
          else
            returnVal[1] += dur[i];

          if (!hour)
            hMsg += dur[i];
        } else if (dur[i] == "M")
          min = false;
        else if (dur[i] == "H")
          hour = true;
          //console.log(dur[i]);
      }

      returnVal[0] = returnVal[0].toString();
      returnVal[1] = returnVal[1].toString();

      //There was no M value
      if (min) {
        returnVal[1] = returnVal[0];
        returnVal[0] = "0";

        //Hours exist
        if (hour) {
          returnVal[0] = hMsg + ":00";
          returnVal[1] = returnVal[1].substring(returnVal[1].length - hMsg.length);
        }
      }

      if (returnVal[1].length === 0)
        returnVal[1] = "00";
      else if (returnVal[1].length === 1)
        returnVal[1] = "0" + returnVal[1];

      if (returnVal[0].length >= 3 && !min) {
        str = returnVal[0].substring(0, returnVal[0].length - 2) + ":" + returnVal[0].substring(returnVal[0].length - 2);
        returnVal[0] = str;
      }

      addLength(returnVal, ind, query);
    } else {
      //console.log(response.statusCode);
      returnVal[0] = "00";
      returnVal[1] = "00";
    }
  });
}

function selectSong(index) {
  index -= 1;
  if (songs.length < 1)
    lastMessage.channel.send("There are no songs to select from");
  else {
    queueStream(songs[index].videoId, songs[index].videoName, songs[index].videoLength);
    songs = [];
  }
}

exports.play = function(query) {
  search(query);

  if (dispatcher[lastMessage.guild.id]) {
    if (dispatcher[lastMessage.guild.id].paused)
      dispatcher[lastMessage.guild.id] = null;
  }
}

function queueStream(videoId, videoName, videoLength) {
  var streamUrl = watchVideoUrl + videoId;
  lastMessage.channel.send(lastMessage.author + " **Queued:\n >** " + videoName);

  queue[lastMessage.guild.id].push({
    'streamUrl': streamUrl,
    'videoName': videoName,
    'videoId': videoId,
    'videoLength': videoLength
  });

  //console.log(queue[lastMessage.guild.id]);

  if (queue[lastMessage.guild.id].length == 1)
    playStream(queue[lastMessage.guild.id][0].streamUrl, queue[lastMessage.guild.id][0].videoName);

}

function playStream(streamUrl, videoName) {
  var streamOptions = {seek: 0, volume: getVolume() / 100};

  if (streamUrl) {
    voiceConnection[lastMessage.guild.id] = findConnectionOfUser(lastMessage.author);

    dispatcher[lastMessage.guild.id] = voiceConnection[lastMessage.guild.id].playStream(stream(streamUrl), streamOptions);
    //console.log(queue[lastMessage.guild.id]);
    lastMessage.channel.send("**Now Playing:\n >** " + videoName);

    dispatcher[lastMessage.guild.id].once('end', (reason) => {
      console.log("Song end: " + reason);
      playNextStreamInQueue(reason);
    });

    dispatcher[lastMessage.guild.id].once('error', (err) => {
      console.log(err);
    });

    if (!voiceConnection[lastMessage.guild.id])
      lastSong[lastMessage.guild.id] = queue[lastMessage.guild.id][0];
  }

}

function setVolume(volume) {
  mVolume = volume;
  if (dispatcher[lastMessage.guild.id])
    dispatcher[lastMessage.guild.id].setVolume(volume / 100);
  lastMessage.channel.send(lastMessage.author + " set volume to: " + volume);
}

function getVolume() {
  if (dispatcher[lastMessage.guild.id])
    return dispatcher[lastMessage.guild.id].volume * 100;

  return mVolume;
}

function playNextStreamInQueue(reason) {
  queue[lastMessage.guild.id].splice(0, 1);

  if (queue[lastMessage.guild.id].length > 0)
    playStream(queue[lastMessage.guild.id][0].streamUrl, queue[lastMessage.guild.id][0].videoName);
  else {
    switch(reason) {
      case 'skip':
        lastMessage.channel.send("Song skipped, queue is empty.");
        break;
      default:
        lastMessage.channel.send("Stream finished! No more songs in queue.");
        break;
    }
  }
}

function skip() {
  if (dispatcher[lastMessage.guild.id] && queue[lastMessage.guild.id].length > 1) {
    dispatcher[lastMessage.guild.id].end();
    lastMessage.channel.send("Song skipped.");
  } else if (dispatcher[lastMessage.guild.id] && queue[lastMessage.guild.id].length == 1)
    dispatcher[lastMessage.guild.id].end(skip);
  else
    lastMessage.channel.send("There's nothing I can skip!");
}

exports.replay = function(ind) {
  if (ind)
    queueStream(queue[lastMessage.guild.id][ind].videoId, queue[lastMessage.guild.id][ind].videoName);
  else if (lastSong[lastMessage.guild.id])
    queueStream(lastSong[lastMessage.guild.id].videoId, lastSong[lastMessage.guild.id].videoName, lastSong[lastMessage.guild.id].videoLength);
  else
    lastMessage.channel.send("I'm not sure what song you want me to play.");
}

exports.clr = function() {
  if (queue[lastMessage.guild.id].length > 0) {
    queue[lastMessage.guild.id] = [queue[lastMessage.guild.id][0]];
    lastMessage.channel.send("Queue cleared.\nKyun~~<3");
  } else
    lastMessage.channel.send("The queue's already empty!");
}

exports.prune = function(ind) {
  if (queue[lastMessage.guild.id].length > 1) {
    let rm = null;
    if (ind && queue[lastMessage.guild.id].length > 1) {
      if (ind < 1 || ind > queue[lastMessage.guild.id].length)
        lastMessage.channel.send("Value must be between 1 and " + queue[lastMessage.guild.id].length);
      else {
        rm = queue[lastMessage.guild.id][ind];
        queue[lastMessage.guild.id].splice(ind, 1);
        lastMessage.channel.send("**Removed:\n >** " + rm.videoName);
      }
    } else {
      rm = queue[lastMessage.guild.id].pop();
      lastMessage.channel.send("**Removed:\n >** " + rm.videoName);
    }
  } else if (queue[lastMessage.guild.id].length <= 1) {
    lastMessage.channel.send("There isn't anything for me to remove!");
  }
}

function stream(streamUrl) {
  return ytdl(streamUrl, {quality: 'highest', filter: 'audioonly'});
}

exports.kill = function(guildId) {
  if (guildId) {
    if (dispatcher[guildId]) {
      dispatcher[guildId].end();
      queue[guildId] = [];
    }
  } else {
    let dis;
    for (dis in dispatcher)
      dispatcher[dis].end();
    queue = [];
  }
}

function findConnectionOfUser(user) {
  for (let connection of bot.voiceConnections.array()) {
    if (connection.channel.members.find('user', user))
      return connection;
  }

  return bot.voiceConnections.first();
}

function isInt(val) {
  return val % 1 === 0;
}


exports.selectSong = selectSong;
exports.setVolume = setVolume;
exports.getVolume = getVolume;
exports.skip = skip;
