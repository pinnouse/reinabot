var request = require('superagent');
const config = require('./config.json');

const API_KEY = config.rapi;

var lastMessage = null;

exports.message = function(msg) {
  lastMessage = msg;
}

function getSummonerByName(region, name) {
  let requestUrl = 'https://na1.api.riotgames.com/lol/summoner/v3/summoners/by-name/' + name + `?api_key=${API_KEY}`;

  return new Promise((resolve, reject) => {
    request(requestUrl, (err, response) => {
      if (!err && response.statusCode === 200) {
  //      console.log(response.body[name]);
        resolve(response.body[name.toLowerCase()]);
      } else {
        console.log(err);
        console.log(response.statusCode);
        reject(err);
      }
    });
  });
}

exports.getSummonerStats = function(region, name) {
  getSummonerByName(region, name)
    .then((summoner) => {
      let requestUrl = 'https://na.api.riotgames.com/api/lol/' + region + '/v1.3/stats/by-summoner/' + summoner.id + `/summary?api_key=${API_KEY}`;
      request(requestUrl, (err, response) => {
        if (!err && response.statusCode === 200) {
          let rankedSoloStats;
          let normalStats;
          for (let i = 0; i < response.body.playerStatSummaries.length; i++) {
            let statSummary = response.body.playerStatSummaries[i];
            if (statSummary.playerStatSummaryType === 'RankedSolo5x5')
              rankedSoloStats = statSummary;
            else if (statSummary.playerStatSummaryType === 'Unranked')
              normalStats = statSummary;
          }

          let rWR = Math.round((rankedSoloStats.wins / (rankedSoloStats.wins + rankedSoloStats.losses)) * 100);
          let rOutput;
          if (rankedSoloStats.wins > 0 || rankedSoloStats.losses > 0) {
            rOutput = rWR + "% winrate | " + rankedSoloStats.wins + "W-" + rankedSoloStats.losses + "L";
          } else {
            rOutput = "This player has no ranked statistics";
          }

          lastMessage.channel.send("**Stats for Summoner:** `" + summoner.name + "`" +
          "\n```xl" +
          "\nLevel: " + summoner.summonerLevel +
          "\nRanked Solo/Duo: " + rOutput +
          "\nNormals: " + normalStats.wins + "W" +
          "```");
        } else {
          console.log(err);
          console.log(response.statusCode);
        }
      })
    });
}

exports.getSummonerGame = function(platformId, summoner) {
  let requestUrl = 'https://na.api.riotgames.com/observer-mode/rest/consumer/getSpectatorGameInfo/' + platformId + '/' + summoner.id;

  request(requestUrl, (err, response) => {
    if (!err && response.statusCode === 200) {
      let participants = response.body.participants;

    } else {
      console.log(err);
      console.log(response.statusCode);
    }
  });
}

exports.getSummonerLeague = function(region, summonerIds) {
  let requestUrl = 'https://na.api.riotgames.com/api/lol/' + region + '/v2.5/league/by-summoner/' + summonerIds;

  request(requestUrl, (err, response) => {
    if (!err && response.statusCode === 200) {
      let entries = response.body.entries;
      let ranks;
      for (sum in entries) {
        ranks.push({
          'rank': sum.rank,
          'lp': sum.leaguePoints
        });
      }
      return ranks;
    } else {
      console.log(err);
      console.log(response.statusCode);
    }
  });
}
