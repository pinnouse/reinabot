const Discord = require('discord.js');
const { RichEmbed } = Discord;
const bot = new Discord.Client();

const MalApi = require('mal-api');

const config = require('./config.json');

var prefix, TOKEN;
if (!config.testing) {
  prefix = config.prod.prefix;
  TOKEN = config.prod.token;
} else {
  prefix = config.test.prefix;
  TOKEN = config.test.token;
}
const help = require('./help.json');

var request = require('superagent');

const musicModule = require('./modules/music.js');
const malModule = require('./modules/mal.js');
const chatModule = require('./modules/chat.js');

const league = require('./league.js');


const mal = new MalApi({
  "username": config.malAuth[0],
  "password": config.malAuth[1]
});

var helpList = new Map();

bot.on('ready', () => {
  console.log(bot.user.username + " is initializing");

  //Init Music
  musicModule.init(bot).then(() => {
    console.log("Music: module initialized");
  }, (reason) => {
    console.error("Could not initialize Music Module: " + reason);
  });

  //Init MAL Integration
  malModule.init(config.malAuth[0], config.malAuth[1]).then(() => {
    console.log("MAL: API verified and module initialized");
  }, (reason) => {
    console.error("Could not initialize MAL-API: " + reason);
  });

  //Read help file and set up 'helpList'
  readHelpFile().then(() => {
    console.log("Help: help file set up");
  }, (reason) => {
    console.error("Could not set up help file");
  });

  console.log("Discord.js bot is initialized.");
  bot.user.setActivity("pinnouse.bitbucket.io", { type: "WATCHING" });
  console.log("Logged in as: " + bot.user.tag);
});

bot.on('disconnect', () => {
  if (bot.voiceConnections.array().length > 0) {
   for (let i = 0; i < bot.voiceConnections.array().length; i++) {
     let vC = bot.voiceConnections.array()[i].channel;
     vC.leave();
   }
  }
  console.log("Bot disconnected");
  process.exit(0);
});

bot.on('message', message => {
  if (message.content.substring(0, prefix.length) === prefix) {
    var arguments = message.content.slice(prefix.length).split(" ");
    var command = arguments.splice(0, 1)[0].toLowerCase();

    console.log("\n\n------------[Command]------------");
    console.log("From: " + message.author.username);
    console.log("=| " + command);
    console.log(arguments);

    ///GENERAL COMMANDS
    
    //Displays a list of commands
    if (command === "help" || command === "h") {
      let pages = "";
      let i = 0;
      helpList.forEach((value, key) => {
        i++;
        pages += `    \`[${i}]\`: \`${key}\`\n`;
      });

      let output = new RichEmbed().setColor("#ccbb64").setAuthor('Reina - Help');
      if (arguments.length < 1) {
        output.setTitle('HELP').setDescription(`Please choose a category from the list using \`${prefix}help\` [category]\n${pages}`);
      } else if (isInt(arguments)) { 
        if (arguments[0] >= 1 && arguments[0] <= help.categories.length) {
          let tempCommands = Array.from(helpList)[arguments[0] - 1];
          output.setTitle(`__${tempCommands[0].toUpperCase()}__`);
          tempCommands[1].forEach((command) => {
            output.addField(command.name, command.description);
          });
        } else {
          output.setColor("#ff3346").setTitle('ERROR').setDescription(`That category doesn't exist, the pages are:\n${pages}`);
        }
      }
      
      message.channel.send(output);
    }

    //Shuts down and disconnects the bot
    else if (command === "kill") {

      if (config.ownerID.includes(message.author.id)) {
        musicModule.quit();

        message.channel.send("またね").then(() => bot.destroy());
      }

    }

    //Makes the bot join the sender's server
    else if (command === "join" || command === "j") {
      if (arguments.length < 1) {
        joinChannelOfUser(message.author, message.channel);
      } else {
        let vc = findVCByName(message, arguments[0]);
        if (vc) {
          joinVoiceChannel(vc, message.channel);
        } else {
          message.channel.send("**WHOOPS!** I can't find the channel you want me to join!");
        }
      }
    }

    //Makes the bot leave the sender's server
    else if (command === "leave") {
      let vChannel = findVCOfUser(message.author);

      if (vChannel) {
        musicModule.leave(message);
        message.channel.send("Leaving: " + vChannel.name).then(() => {
          vChannel.leave().then(() => {
            message.channel.send("Left: " + vChannel.name);
          }, (reason) => {
            message.channel.send("Could not leave: " + vChannel.name + "\nReason: " + reason);
          });
        });
      } else
        message.channel.send("Could not find the voice channel you are connected to. You must be in the voice channel that you want me to leave.");
    }

    //Deletes messages from text channel
    else if (command === "delete" || command === "del") {
      if (arguments.length > 0) {
        if (isInt(arguments[0])) {
          if (arguments[0] > 100 || arguments[0] < 2)
            message.channel.send("Must be between 2 and 100 messages to delete.\nUsage: `" + prefix + "delete <2-100>` or `" + prefix + "del <2-100>`");
          else {
            message.delete();
            message.channel.bulkDelete(arguments[0], true);
          }
        }
      }
      else {
        message.channel.send("Please specify how many messages you wish to delete.\nUsage: `" + prefix + "delete <2-100>` or `" + prefix + "del <2-100>`");
      }
    }

    ///MAL COMMANDS

    //Searches MyAnimeList for search term(s)
    else if (command === "myanimelist" || command === "mal") {
      if (arguments.length > 0) {
        malModule.command(message, arguments.splice(0, 1)[0], arguments);
      } else
        message.channel.send("Please enter in search queries.\nUsage: `" + prefix + "myanimelist <query>` or `" + prefix + "mal <query>`");
    }

    ///MUSIC COMMANDS

    //Plays a song with requested search terms
    else if (command === "play" || command === "p") {
      if (arguments.length > 0) {
        if (!findVCOfUser(message.author).members.find('user', bot.user))
          joinChannelOfUser(message.author, message.channel).then(musicModule.search(message, arguments.join(" ")));
        else
          musicModule.search(message, arguments.join(" "));
      }
      else
        message.channel.send("Not enough parameters specified to find song.");
    }

    //Selects a song from a list if exists
    else if (command === "select" || command === "sel") {
      musicModule.select(message, arguments[0]);
    }

    else if (command === "pause") {
      musicModule.pausePlayback(message);
    }

    else if (command === "resume") {
      musicModule.resumePlayback(message);
    }

    //Skips the current stream if playing
    else if (command === "skip" || command === "s") {
      musicModule.skip(message);
    }

    //Change/Display current volume
    else if (command === "volume" || command === "vol" || command === "v") {
      musicModule.volume(message, arguments.join(" "));
    }

    //Displays current queue for songs
    else if (command === "queue" || command === "q") {
      musicModule.getQueue(message);
    }

    //Replays the last queued song
    else if (command === "replay" || command === "r") {
      musicModule.replay(message, arguments.join(" "));
    }

    //Purges the queue of all songs
    else if (command === "clear" || command === "clr") {
      musicModule.clearQueue(message);
    }

    //Removes the last song added to queue
    else if (command === "prune" || command === "remove") {
      musicModule.prune(message, arguments[0]);
    }

    //Allows for looping capabilities
    else if (command === "loop") {
      musicModule.loop(message, arguments[0]);
    }

    //Pauses playback and remembers the position
    else if (command === "pause") {
      musicModule.pause(message);
    }

    //Resumes playback at the position time if played was paused
    else if (command === "resume") {
      musicModule.resume(message);
    }

    ///LEAGUE COMMANDS

    //Stats of the specified summoner
    else if (command === "stats") {
      if (arguments.length < 1)
        message.channel.send("Please specify the summoner.\nUsage: `" + prefix + "stats <summoner>`");
      else {
        league.message(message);
        league.getSummonerStats('na', arguments.join(""));
      }
    }

/*
    else if (command == "game") {
      if (arguments.length < 1)
        message.channel.send("Please specify the summoner.\nUsage: `" + prefix + "game <summoner>`");
      else
        league.getSummonerStats(league.getSummonerByName('na', arguments.join(" ")));
    }*/

    //Unrecognized command
    else {
      console.log("Command not recognized.");
    }

  } else if (message.author.id != bot.user.id) {
    console.log("\n\n------------[Message]------------");
    console.log("From: " + message.author.username);
    console.log("=| " + message.content);
    
    if (message.content.startsWith(`<@${bot.user.id}>`)) {
      chatModule.sendMessage(bot, message, message.content.slice(`<@${bot.user.id}>`.length));
    }
  }
});

function joinChannelOfUser(user, msgChannel) {
  let channel;
  let connectedUser;
  for (channel of user.client.channels.array()) {
    if (channel.type == 'voice' && channel.joinable) {
      for (connectedUser of channel.members.array()) {
        if (connectedUser.id === user.id) {
          return joinVoiceChannel(channel, msgChannel);
        }
      }
    }
  }
  msgChannel.send("I'm unable to connect to the same voice channel as you.\nCheck if I'm in the same server and if I have permissions to join that server please~")
    .then(() => {
      return Promise.reject('noChannel');
    });
}

function findVCByName(msg, channelName) {
  let guild = msg.guild;
  let guildChannel = guild.channels.find(val => val.name === channelName && val.type === 'voice');
  return guildChannel;
}

function joinVoiceChannel(vChannel, msgChannel) {
  return new Promise((resolve, reject) => {
    if (vChannel && vChannel.type === 'voice') {
      if (vChannel.joinable) {
        msgChannel.send(`Attempting to join: \`${vChannel.name}\``);
        vChannel.join()
          .then(() => {
            msgChannel.send(`Successfully joined: \`${vChannel.name}\``);
            resolve('success');
          })
          .catch(reason => {
            msgChannel.send(`Failed to join: \`${vChannel.name}\``);
            console.log('Failed because of: ' + reason);
            reject(reason);
          });
      } else {
        msgChannel.send("**WHOOPS!** It appears that the voice channel you want me to join is not joinable.")
          .then(() => {
            reject('notJoinable');
          });
      }
    } else {
      msgChannel.send("**WHOOPS!** The requested server doesn't exist or isn't a voice channel.")
        .then(() => {
          reject('noChannel');
        });
    }
  });
}

function findVCOfUser(user) {
  let channel;
  let connectedUser;
  for (channel of user.client.channels.array()) {
    if (channel.type == 'voice') {
      for (connectedUser of channel.members.array()) {
        if (connectedUser.id === user.id)
          return channel;
      }
    }
  }
  return null;
}

function readHelpFile() {
  try {
    var tempCommandHolder;
    for (var i = 0; i < help.categories.length; i++) {
      tempCommandHolder = [];
      for (var j = 0; j < help.categories[i].commands.length; j++) {
        let tempCommand = help.categories[i].commands[j]; //Set temporary variable to hold this iteration's command
    
        //Temporary holder for description of command
        let temp = `${tempCommand.description}\n    Usage:`; //Print command name and description on one line
    
        for (let k = 0; k < tempCommand.usage.length; k++) { //Output the usage on a seperate line (seperated by "or")
          if (k > 0)
            temp += " or";
          temp += ` \` ${config.prefix}${tempCommand.usage[k]} \``;
        }
    
        let x;
        if (tempCommand.reqArgs) { //Check for required arguments
          for (x = 0; x < tempCommand.reqArgs.length; x++) {
            temp += ` <${tempCommand.reqArgs[x]}>`;
          }
        }
        if (tempCommand.optArgs) { //Check for optional arguments
          for (x = 0; x < tempCommand.optArgs.length; x++) {
            temp += ` [${tempCommand.optArgs[x]}]`;
          }
        }
  
        tempCommandHolder[j] = {
          "name": tempCommand.name,
          "description": temp
        };
      }

      helpList.set(help.categories[i].category, tempCommandHolder);
    }
  } catch(e) {
    return Promise.reject(e);
  }
  return Promise.resolve();
}

function isInt(arg) {
  return arg % 1 === 0;
}

console.log("Logging in");
bot.login(TOKEN).catch((e) => {
  console.log('connecting error: ' + e.code);
});