# Reina Bot Readme

### Website
https://pinnouse.bitbucket.io

## About

Reina Bot is a simple [Discord](https://discordapp.com) bot written in [NodeJS](https://nodejs.org) using the [Discord.JS](https://discord.js.org) library.

## Features

Main

- Music
- MyAnimeList

Currently in progress

- League Of Legends integration
- Osu! integration
- moderation commands

## Contact

- [Email](mailto:futawong@owo.soy)
- [Twitter](https://twitter.com/pinnouse)

## Installation

All platforms

- [NodeJS](https://nodejs.org)
- [FFMPEG](https://ffmpeg.org)
- [Python](https://python.org)

## Running

Windows | Mac
------- | ---
Open a `"Command Prompt"` or `"Powershell"` window and change to the bot's directory (here) | Open up `"Terminal"` on your mac
(Optional step - faster step 1) Shift + right-click the explorer and click the option `"Open command window here"` | Redirect the terminal to the path/directory of where Reina is stored
In the prompt, enter in "npm start" without the quotation marks to run the bot | Enter in `"npm start"` to run the bot

## Help
If there are any issues, make a document and record the issues or send me an email regarding the problem.
When doing so, please include as much information as you can about the specified error.