Reina Bot Readme


Adding Reina to a Server:

Use the following link and add her to the desired server (note: you must have permission to add users and bots to the server if you want to add Reina)
https://discordapp.com/api/oauth2/authorize?client_id=264699239102283776&scope=bot&permissions=0


Prerequisites:

Windows
 - FFMPEG (https://github.com/Soundofdarkness/FFMPEG-Inst/releases/download/1.4/FFMPEG.Installer.exe)
 - Node.JS (https://nodejs.org/dist/v7.4.0/node-v7.4.0-x64.msi)

Mac
 - FFMPEG (http://www.ffmpegmac.net/resources/Lion_Mountain_Lion_Mavericks_Yosemite_El-Captain_08.12.2016.zip)
 - Node.JS (https://nodejs.org/dist/v7.4.0/node-v7.4.0.pkg)



Running:

Windows
 Option 1 - run program "run.bat"
 Option 2 - shift + right-click the explorer and click the option "Open command window here"
	in the prompt, enter in "node bot.js" without the quotation marks to run the bot

Mac
 Open up "Terminal"
 Redirect the terminal to the path/directory of where reina is stored
 Enter in "node bot.js" to run the bot


If there are any issues, make a document and record the issues or send me an email regarding the problem.
When doing so, please include as much information as you can about the specified error and perhaps the error code. 