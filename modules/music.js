const { RichEmbed } = require('discord.js');
var request = require('superagent');
const ytdl = require('ytdl-core');

const config = require('../config.json');

const GAPI = config.gapi;
const PREFIX = config.prefix;

const WATCH_VIDEO_URL = 'https://www.youtube.com/watch?v=';

const MAX_VOLUME = 200;

const QUEUE_MAX_LENGTH = 100;

const LoopTypes = {
  NONE: {value: 0, type: "None"},
  SINGLE: {value: 1, type: "Single Track"},
  LIST: {value: 2, type: "List"}
}

var BOT; //Global instance of bot

var musicPlayers = new Map();

class MusicPlayer {

  constructor() {
    this.volume = 100;

    this.vidList = [];

    this.queue = [];

    this.message = null;

    this.voiceConnecton = null;

    this.lastSong = null;

    this.seek = 0; //If the player pauses, where to resume the track

    this.loopType = LoopTypes.NONE;
  }

  search(query) {
    if (this.queue.length <= QUEUE_MAX_LENGTH) {
      this.message.channel.startTyping();
      console.log("query: " + query);
      let url = 'https://www.googleapis.com/youtube/v3/search' + `?part=snippet&maxResults=5&q=${escape(query)}&key=${GAPI}`;
      request.get(url, (err, response) => {
        if (!err && response.statusCode === 200) {
          let body = response.body;
          if (body.items.length < 1) {
            this.message.channel.send("I couldn't find anything on YouTube that matches that.");
            this.message.channel.stopTyping();
            return;
          }

          this.vidList = [];
          for (let i = 0; i < body.items.length; i++) {
            let item = body.items[i];
            if (item.id.kind === 'youtube#video') {
              this.vidList.push({
                'videoId': item.id.videoId,
                'videoName': item.snippet.title,
                'videoLength': "00:00",
                'queuer': this.message.author
              });
            }
          }

          this.getDuration(query);
        } else {
          this.message.channel.send("Um... Something happened, I can't access the Google API servers.").then(this.message.channel.stopTyping());
          console.log("Error: " + err + "\nCode: " + response.statusCode);
          return;
        }
      });
    } else {
      this.message.channel.send("Onii chan, やめろ. I can only remember " + QUEUE_MAX_LENGTH + " songs at a time.");
    }
  }

  getDuration(query) {
    let requestPromises = [];
    for (let i = 0; i < this.vidList.length; i++) {
      requestPromises.push(new Promise((resolve, reject) => {
        let url = 'https://www.googleapis.com/youtube/v3/videos' + `?id=${this.vidList[i].videoId}&key=${GAPI}&part=contentDetails`;
        request.get(url, (err, response) => {
          if (!err && response.statusCode === 200) {
            let duration = response.body.items[0].contentDetails.duration;
            resolve(parseTime(duration.substring(2)));
          } else {
            this.message.channel.send("Um... Something happened, I can't access the Google API servers.");
            console.log("Error: " + err + "\nCode: " + response.statusCode);
            reject(response.statusCode);
          }
        });
      }));
    }

    Promise.all(requestPromises).then(durations => {
      for (let i = 0; i < this.vidList.length; i++) {
        this.vidList[i].videoLength = durations[i];
      }

      this.listSongs(query);
    }, reason => {
      this.message.channel.stopTyping();
      console.log(reason);
    });
  }

  listSongs(query) {
    let output = new RichEmbed().setAuthor('Reina - Search').setTitle('RESULTS').setColor(0xffffff);
    let desc = `**Results for:** ${query}\n**Use** \` ${PREFIX}select <1-${this.vidList.length}> \` or \` ${PREFIX}sel <1-${this.vidList.length}> \` **to choose the song to play.**`;
    if (this.vidList.length > 1) {
      for (let i = 0; i < this.vidList.length; i++) {
        desc += "\n `[`[`" + (i + 1) + "`](" + WATCH_VIDEO_URL + this.vidList[i].videoId + ")`]` `" + this.vidList[i].videoLength + " ` - " + this.vidList[i].videoName;
      }
    } else {
      desc = `**Result for:** ${query} \n**Use** \` ${PREFIX}select \` or \` ${PREFIX}sel \` **if this is the song you were looking for.**`
        + "\n `[`[`link`](" + WATCH_VIDEO_URL + this.vidList[0].id + ")`]`"
        + `\`${this.vidList[0].videoLength} \` - ${this.vidList[0].videoName}`;
    }
    output.setDescription(desc);
    this.message.channel.send(output).then(this.message.channel.stopTyping());
  }

  selectSong(value) {
    if (this.vidList.length <= 0) {
      this.message.channel.send("There are no songs to select from.");
    } else if (this.vidList.length === 1 || !value) {
      this.queueSong(this.vidList[0]);
      if (this.message.channel.typing)
        this.message.channel.stopTyping();
    } else if (isInt(value)) {
      if (value >= 1 && value <= this.vidList.length) {
        this.queueSong(this.vidList[value - 1]);
        if (this.message.channel.typing)
          this.message.channel.stopTyping();
      } else {
        this.message.channel.send("Sorry the value must be between 1 and " + this.vidList.length);
      }
    } else {
      this.message.channel.send("Numbers only please.");
    }
  }

  queueSong(song, output = true) {
    this.vidList = [];
    this.queue.push(song);
    this.lastSong = song;
    if (this.queue.length === 1) {
      this.playSong(song);
    } else {
      if (output)
        this.message.channel.send(new RichEmbed().setColor("#ccaa44").setTitle("Added to Queue:").setDescription(song.videoName).addField("Queued by:", song.queuer, false));
    }
  }

  playSong(song, reason = 'user') {

    if (!song) {
      switch(reason) {
        case 'skip':
        this.message.channel.send("Song skipped, queue is empty.");
        break;
        case 'kill':
        //Do nothing if the bot is told to kill
        break;
        default:
        this.message.channel.send("Stream finished! No more songs in queue.");
        break;
      }
      return;
    }

    var streamUrl = WATCH_VIDEO_URL + song.videoId;
    var streamOptions = {seek: 0};

    if (streamUrl) {

      if (!this.voiceConnection)
        this.voiceConnection = findVoiceConnectionOfUser(this.message.author);

      if (this.voiceConnection) {
        var dispatcher = this.voiceConnection.playStream(stream(streamUrl), streamOptions)
          .once('end', (reason) => {
            console.log("Song end: " + reason);
            if (reason != 'undefined') {
              switch(this.loopType) {
                case LoopTypes.NONE:
                this.playSong(this.queue[1], reason);
                this.queue.splice(0, 1);
                break;
                case LoopTypes.SINGLE:
                if (reason === 'skip') {
                  this.playSong(this.queue[1], reason);
                  this.queue.splice(0, 1);
                } else
                  this.playSong(this.queue[0], reason);
                break;
                case LoopTypes.LIST:
                this.queueSong(this.queue[0], false);
                this.playSong(this.queue[1], reason);
                this.queue.splice(0, 1);
                break;
              }
            }
          })
          .once('error', (error) => {
            console.log("Playback error: " + error);
          });

          let output = new RichEmbed().setAuthor('Reina - Now Playing').addField("Name", song.videoName).addField("Queued by", song.queuer, false);
          switch(reason) {
            case 'skip':
            output.setColor("#00ffd0").setAuthor('Reina - Song Skipped');
            break;
            default:
            output.setColor("#00ff33");
            break;
          }

          this.message.channel.send(output);

          dispatcher.setVolume(this.getVolume / 100);
      } else {
        this.voiceConnection = reconnectToChannel(this.message.author);
        console.log("Could not obtain voice connection of user");
      }
    }
  }

  skipSong() {
    if (this.voiceConnection) {
      //this.playSong(this.queue[0], 'skip')
      this.voiceConnection.dispatcher.end('skip');
    } else
      this.message.channel.send("I am not connected to a voice channel yet, there is nothing for me to skip");
  }

  vol(args) {
    if (args) {
      if (isInt(args)) {
        if (parseInt(args) > MAX_VOLUME)
          this.message.channel.send("Max volume is: " + MAX_VOLUME + "%");
        else if (parseInt(args) < 0)
          this.message.channel.send("The minimum volume is: 0%");
        else {
          this.setVolume(parseInt(args));
          this.message.channel.send(this.message.author + " set the volume to: " + args + "%");
        }
      } else {
        this.message.channel.send("Numbered values please");
      }
    } else {
      this.message.channel.send("Volume is: " + this.getVolume + "%");
    }
  }

  replay(value) {
    if (this.queue.length > 0) {
      if (!value) {
        if (this.lastSong)
          this.queueSong(this.lastSong);
        else
          this.message.channel.send("Sorry, I'm not sure what the previous song that played was.");
      } else if (isInt(value)) {
        if (value > this.queue.length - 1 || value < 0)
          this.message.channel.send("Value must be between 0 and " + this.queue.length - 1);
        else
          this.queueSong(this.queue[value]);
      } else {
        this.message.channel.send("Sorry, I don't understand that, numbered values only please.");
      }
    } else {
      if (this.lastSong)
        this.queueSong(this.lastSong);
      else
        this.message.channel.send("Sorry, I'm not sure what the previous song that played was.");
    }
  }

  getQueue() {
    if (this.queue.length > 0) {
      let output = `**Now Playing:**\n  **>** \` ${this.queue[0].videoLength} \` - \` ${this.queue[0].videoName} \`\n **Queued by:** ${this.queue[0].queuer.username}`
        + `\n**Loop type:** *\` ${this.loopType.type} \`*`;
      if (this.queue.length > 1) {
        output += "\n\n**Currently queued songs:**";
        for (let i = 1; i < this.queue.length; i++) {
          output += "\n`[" + i + "]` `" + this.queue[i].videoLength + "` - " + this.queue[i].videoName;
        }
      } else {
        output += "\n\n**There are no other songs queued**";
      }

      this.message.channel.send(output);
    } else {
      this.message.channel.send("The queue is empty, nothing to display.");
    }
  }

  clearQueue(output = true) {
    if (this.queue.length > 1) {
      this.queue = [this.queue[0]];
      if (output)
        this.message.channel.send("Cleared the queue ~⋆");
    } else if (output) {
      this.message.channel.send("There are no songs to clear.");
    }
  }

  prune(value) {
    if (value) {
      if (isInt(value)) {
        if (this.queue.length > 1 && value < this.queue.length) {
          if (value > 0) {
            let removed = this.queue[value];
            this.queue.splice(value, 1);
            this.message.channel.send("**Song Removed**\n **>** " + removed.songName + "\n**Removed by: " + this.message.author);
          } else {
            this.message.channel.send("Positive values only please.");
          }
        } else if (this.queue.length > 1) {
          if (this.queue.length == 2) {
            this.message.channel.send("Sorry, there is only 1 song in the queue that you can remove.");
          } else {
            this.message.channel.send("Sorry, there are only " + (this.queue.length - 1) + " songs in the queue that you can remove.");
          }
        } else {
          this.message.channel.send("Sorry, the queue is empty, there is nothing for me to remove.");
        }
      } else {
        this.message.channel.send("Numbered values only please.");
      }
    } else {
      let removed = this.queue.pop();
      this.message.channel.send("**Song Removed**\n **>** " + removed.songName + "\n**Removed by: " + this.message.author);
    }
  }

  loop(type) {
    if (type) {
      switch(type) {
        case 'none':
        this.loopType = LoopTypes.NONE;
        this.message.channel.send(`Set the loop mode to: \`${this.loopType.type}\``);
        break;
        case 'single':
        this.loopType = LoopTypes.SINGLE;
        this.message.channel.send(`Set the loop mode to: \`${this.loopType.type}\``);
        break;
        case 'list':
        this.loopType = LoopTypes.LIST;
        this.message.channel.send(`Set the loop mode to: \`${this.loopType.type}\``);
        break;
        default:
        this.message.channel.send(`Sorry, that's not a valid option:\n \`${PREFIX}loop <none|single|list>\``);
        break;
      }
    } else 
      this.message.channel.send(`Please enter in a parameter:\n \`${PREFIX}loop <none|single|list>\``);
  }

  quit() {
    this.clearQueue(false);
    if (this.voiceConnection && this.voiceConnection.dispatcher)
      this.voiceConnection.dispatcher.end('kill');
  }

  pausePlayback() {
    if (this.voiceConnection && this.voiceConnection.dispatcher && this.voiceConnection.dispatcher.stream) {
      if (!this.voiceConnection.dispatcher.paused) { //Pauses playback if not paused
        this.voiceConnection.dispatcher.pause();
        this.message.channel.send("Paused playback");
      } else { //Resumes playback if paused
        this.voiceConnection.dispatcher.resume();
        this.message.channel.send("Resumed playback");
      }
    } else
    this.message.channel.send("Sorry, I couldn't pause the music, are you sure there's something playing right now?");
  }

  resumePlayback() {
    if (this.voiceConnection && this.voiceConnection.dispatcher && this.voiceConnection.dispatcher.stream) {
      if (this.voiceConnection.dispatcher.paused) { //If paused, resume playback
        this.voiceConnection.dispatcher.resume();
        this.message.channel.send("Resumed playback");
      } else
        this.message.channel.send("There isn't anything paused for me to resume");
    } else
      this.message.channel.send("Sorry, I couldn't resume the music, are you sure there's something playing right now?");
  }

  setMessage(message) {
    this.message = message;
  }

  setVolume(volume) {
    this.volume = volume;
    if (this.voiceConnection)
      this.voiceConnection.dispatcher.setVolume(this.volume / 100);
  }

  get getVolume() {
    return this.volume;
  }
}

function parseTime(time) {
  let days = "00";
  let hours = "00";
  let minutes = "00";
  let seconds = "00";

  let returnVal = "00:00";

  let token = "";
  for (let i = 0; i < time.length; i++) {
    switch(time[i]) {
      case 'D':
        days = token;
        token = "";
        break;
      case 'H':
        hours = token;
        token = "";
        break;
      case 'M':
        minutes = token;
        token = "";
        break;
      case 'S':
        seconds = token;
        token = "";
        break;
      default:
        token += time[i];
        break;
    }
  }

  if (seconds.length === 1)
    seconds = "0" + seconds;

  if (minutes.length === 1)
    minutes = "0" + minutes;

  returnVal = minutes + ":" + seconds;

  if (hours != "00" || days != "00") {
    if (hours.length === 1)
      hours = "0" + hours;

    returnVal = hours + ":" + returnVal;
  }

  if (days != "00")
    returnVal = days + ":" + returnVal;

  return returnVal;
}

///DECLARE MODULE FUNCTIONS
module.exports = {
  init(client) {
    return new Promise((resolve, reject) => {
      BOT = client;
      resolve();
    });
  },

  search(message, query) {
    getMusicPlayer(message).search(query);
  },

  select(message, value) {
    getMusicPlayer(message).selectSong(value);
  },

  skip(message) {
    getMusicPlayer(message).skipSong();
  },

  volume(message, args) {
    getMusicPlayer(message).vol(args);
  },

  replay(message, value) {
    getMusicPlayer(message).replay(value);
  },

  getQueue(message) {
    getMusicPlayer(message).getQueue();
  },

  clearQueue(message) {
    getMusicPlayer(message).clearQueue();
  },

  prune(message, value) {
    getMusicPlayer(message).prune(value);
  },

  loop(message, type) {
    getMusicPlayer(message).loop(type);
  },

  leave(message) {
    getMusicPlayer(message).quit();
  },

  pausePlayback(message) {
    getMusicPlayer(message).pausePlayback();
  },

  resumePlayback(message) {
    getMusicPlayer(message).resumePlayback();
  },

  quit() {
    quitAll();
  }

}

function stream(streamUrl) {
  return ytdl(streamUrl, {quality: 'highestaudio', filter: 'audioonly'});
}

function getMusicPlayer(message) {
  let guild = message.channel.guild;
  if (guild.available) {
    if (musicPlayers.get(guild.id)) {
      musicPlayers.get(guild.id).setMessage(message);
      return musicPlayers.get(guild.id);
    }

    var musicPlayer = new MusicPlayer();
    musicPlayer.setMessage(message);
    musicPlayers.set(guild.id, musicPlayer);
    return musicPlayer;
  } else
    console.log("Guild is not available");
}

function quitAll() {
  for (let i = 0; i < musicPlayers.length; i++) {
    musicPlayers[i].quit();
  }
}

function getChannelOfUser(user) {
  let channel;
  let connectedUser;
  for (channel of user.client.channels.array()) {
    if (channel.type == 'voice') {
      for (connectedUser of channel.members.array()) {
        if (connectedUser.id === user.id)
          return channel;
      }
    }
  }
  return null;
}

function findVoiceConnectionOfUser(user) {
  for (let connection of BOT.voiceConnections.values()) {
    if (connection.channel.members.find('user', user))
      return connection;
  }

  return null;
}

function reconnectToChannel(user) {
  let channel = getChannelOfUser(user);
  if (channel) {
    channel.leave();
    channel.join();
    return findVoiceConnectionOfUser(user);
  } else
    console.log("Errors all around");
}

function isInt(val) {
  return val % 1 === 0;
}
