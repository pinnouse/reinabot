const MalApi = require('mal-api');
const config = require('../config.json');
const { RichEmbed } = require('discord.js');

const PREFIX = config.prefix;

var mal;

var list = new Map();

///MODULE FUNCTIONS
module.exports = {

  init(username, password) {
    mal = new MalApi({
      "username": username,
      "password": password
    });
    return mal.account.verifyCredentials();
  },

  command(message, command, arguments) {
    switch(command) {
      
      case 'search':
      getAnime(message, arguments.join("_"));
      break;

      case 'select':
      case 'sel':
      select(message, arguments[0]);
      break;

      case 's':
      if (isInt(arguments[0]) && list.get(message.guild.id) && list.get(message.guild.id).length > 0)
        select(message, arguments[0]);
      else
        getAnime(message, arguments.join("_"));
      break;

    }
  }


}

function getAnime(message, query) {
  console.log('query');
  mal.anime.searchAnime(query)
    .then(res => {
      list.set(message.guild.id, []);
      //console.log(res);
      if (res.id) {
        message.channel.send(getAnimeEmbed(res));
        return;
      }

      let len = 5;
      if (res.length < 5)
        len = res.length;

      list.set(message.guild.id, res);

      let output = new RichEmbed().setAuthor('Reina - Anime Search').setColor(0x2e51a2).setTitle('Results');
      let desc = `**Multiple results for:** ${query}\n**Use** \` ${PREFIX}mal select|sel|s <1-${len}> \` **to choose an anime.**`;
      
      for (let i = 0; i < len; i++) {
        desc += `\n \`[ \`[\`${i+1}\`](https://myanimelist.net/anime/${res[i].id})\` ]\` - \` ${res[i].title} \``;
      }

      output.setDescription(desc);
      message.channel.send(output);
    })
    .catch(err => {
      console.error(err);
      message.channel.send("Could not find the anime you were looking for")
    });
}

function select(message, choice) {
  if (list && list.get(message.guild.id).length > 0) {
    message.channel.send(getAnimeEmbed(list.get(message.guild.id)[choice - 1]));
    list.set(message.guild.id, []);
  }
}

function getAnimeEmbed(res) {
  let url = `https://myanimelist.net/anime/${res.id}`;
  let title = removeSpecialChars(res.title);
  let description = removeSpecialChars(res.synopsis);
  let type = res.type;
  let score = res.score;
  let episodes = res.episodes;
  let imageURL = res.image;
  let airDates = removeSpecialChars(
    `${res.start_date} to ${(res.end_date === '0000-00-00') ? res.end_date : 'present'}`
  );
  return new RichEmbed()
  .setColor(0x2e51a2)
  .setAuthor('Reina - Anime')
  .setTitle(title)
  .setURL(url)
  .setThumbnail(imageURL)
  .setDescription(description)
  .addField("Type:", type, true)
  .addField("Rating:", `${score} / 10`, true)
  .addField("Episodes:", episodes, true)
  .addField("Aired:", airDates, true);
}

function removeSpecialChars(str) {
  return str.replace(/<(?:.|\n)*?>/gm, '').replace(/&#039;/gm, '\'').replace("[Written by MAL Rewrite]", '').replace(/&quot;/gm, '\"');
}

function isInt(arg) {
  return arg % 1 === 0;
}