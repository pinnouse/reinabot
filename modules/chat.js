const request = require('superagent');
const { RichEmbed } = require('discord.js')
const config = require('../config.json')
const Parser = require('rss-parser');
var parser = new Parser();
const data = require('./data.json')

module.exports = {
    sendMessage(bot, message, sentence) {
        message.channel.startTyping()
        request.get(`http://${config.chatUrl}/api?s=${sentence}`, (err, response) => {
            if (!err && response.statusCode === 200) {
                var body = JSON.parse(response.text)
                var reply = body.out_sentence.replace(/\<[A-Z]+\>/gm, '');
                if (reply.includes("$time"))
                    time(message);
                else if (reply.includes("$news"))
                    news(message);
                else if (reply.includes("$pun"))
                    pun(message);
                else if (reply.includes("$name"))
                    name(bot, message);
                else
                    message.reply(reply);
                // console.log(body);
                message.channel.stopTyping();
            } else {
                message.channel.send("Sorry, there was an error trying to contact the chatbot module.");
                console.log("Error: " + err + "\nCode: " + response.statusCode);
                message.channel.stopTyping();
            }
        });
    }
}

function time(message) {
    var now = new Date();
    var dd = now.getDate();
    var mm = now.getMonth()+1; //Starts at 0
    var yyyy = now.getFullYear();

    if (dd < 10)
        dd = '0' + dd;
    if (mm < 10)
        mm = '0' + mm;

    var timezoneOffset = now.getTimezoneOffset();
    var tZoneHours = Math.floor(timezoneOffset);
    var tZoneMins = (timezoneOffset - tZoneHours) * 60;

    message.reply(`${mm}/${dd}/${yyyy} ${now.getHours()}:${now.getMinutes()} Timezone Offset by: ${tZoneHours}:${tZoneMins}`);
}

var rssNetworks = [
    "http://www.cbc.ca/cmlink/rss-topstories",
    "https://globalnews.ca/feed/"
];
function news(message) {
    (async() => {
            let body = await parser.parseURL(rssNetworks[0]);
            console.log(body);
            let embed = new RichEmbed()
            body.items.forEach((item, index) => {
                console.log(item.title + ':' + item.link);
            });
            message.channel.send(embed);
        }
    )();
}

function pun(message) {
    message.reply(data.puns[Math.floor(Math.random() * data.puns.length)]);
}

function name(bot, message) {
    message.reply(bot.user.tag);
}